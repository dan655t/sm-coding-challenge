﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using sm_coding_challenge.Consts;
using sm_coding_challenge.Models;
using sm_coding_challenge.Services.DataProvider;

namespace sm_coding_challenge.Controllers
{
    public class HomeController : Controller
    {

        private IDataProvider _dataProvider;
        public HomeController(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider ?? throw new ArgumentNullException(nameof(dataProvider));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Player(string id)
        {
            return Json(await _dataProvider.GetPlayerById(id));
        }

        [HttpGet]
        public async Task<IActionResult> Players(string ids)
        {
            var idList = ids.Split(',');
            var returnList = new List<PlayerModel>();
            foreach (var id in idList)
            {
                // Duplicates should be removed from the existing GetPlayers result(s)
                if (returnList.Any(x => x?.Id == id))
                {
                    continue;
                }
                returnList.Add(await _dataProvider.GetPlayerById(id));
            }
            return Json(returnList);
        }

        // I'm not sure why this is called LatestPlayers
        [HttpGet]
        public async Task<IActionResult> LatestPlayers(string ids)
        {
            var idList = ids.Split(',');
            var playerList = new List<PlayerModel>();
            foreach (var id in idList)
            {
                playerList.Add(await _dataProvider.GetPlayerById(id));
            }

            var returnList = new 
            {
                receiving = playerList.Where(x => x?.Position == PlayerPosition.WR.ToString()),
                rushing = playerList.Where(x => x?.Position == PlayerPosition.RB.ToString()),
                passing = playerList.Where(x => x?.Position == PlayerPosition.QB.ToString()),
                kicking = playerList.Where(x => x?.Position == PlayerPosition.K.ToString())
            };

            return Json(returnList);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
