using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using sm_coding_challenge.Models;
using sm_coding_challenge.Models.Extensions;

namespace sm_coding_challenge.Services.DataProvider
{
    public class DataProviderImpl : IDataProvider
    {
        private Lazy<DataResponseModel> _dataResponseLazy = new Lazy<DataResponseModel>(() => getDataResponseModel());
        private DataResponseModel _dataResponse => _dataResponseLazy.Value;

        private static DataResponseModel getDataResponseModel()
        {
            var handler = new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
            using (var client = new HttpClient(handler))
            {
                try
                {
                    var response = client.GetAsync("https://gist.githubusercontent.com/RichardD012/a81e0d1730555bc0d8856d1be980c803/raw/3fe73fafadf7e5b699f056e55396282ff45a124b/basic.json").Result;
                    
                    // test invalid url
                    // var response = client.GetAsync("https://gist.githubusercontent.com/RichardD012/a81e0d1730555bc0d8856d1be980c803/raw/3fe73fafadf7e5b699f056e55396282ff45a124bz/basic.json").Result;

                    // if async we would await ReadAsStringAsync() and remove ".Result"
                    var stringData = response.Content.ReadAsStringAsync().Result;

                    return JsonConvert.DeserializeObject<DataResponseModel>(stringData, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.Auto });
                }
                catch (Exception)
                {
                    return new DataResponseModel();
                }
            }
        }

        // no longer needs to be async since we lazy cache the response
        public async Task<PlayerModel> GetPlayerById(string id)
        {
            if (!_dataResponse.IsValid())
            {
                return null;
            }

            PlayerModel playerModelMatch = null;
            if ((playerModelMatch = _dataResponse.Rushing.FirstOrDefault(player => player.Id.Equals(id))) != null
            || (playerModelMatch = _dataResponse.Passing.FirstOrDefault(player => player.Id.Equals(id))) != null
            || (playerModelMatch = _dataResponse.Receiving.FirstOrDefault(player => player.Id.Equals(id))) != null
            || (playerModelMatch = _dataResponse.Kicking.FirstOrDefault(player => player.Id.Equals(id))) != null)
            {
                return playerModelMatch;
            }
            else
            {
                return null;
            }
        }
    }
}
