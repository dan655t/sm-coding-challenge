using System.Runtime.Serialization;

namespace sm_coding_challenge.Models
{
    [DataContract]
    public class PlayerModel
    {
        [DataMember(Name = "player_id")]
        public string Id { get; set; }

        [DataMember(Name = "entry_id")]
        public string EntryId { get; set; }        

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "position")]
        public string Position { get; set; }

        //  Add missing player attributes to the fetch so all data from the data provider is returned to the front-end
        [DataMember(Name = "yds")]        
        public string Yards { get; set; }
        
        [DataMember(Name = "tds")]
        public string Touchdowns { get; set; }
        
        [DataMember(Name = "rec")]
        public string Receptions { get; set; }
        
        [DataMember(Name = "att")]
        public string Attempts { get; set; }

        [DataMember(Name = "fum")]
        public string Fumbles { get; set; }
        
        [DataMember(Name = "cmp")]
        public string Completions { get; set; }
        
        [DataMember(Name = "int")]
        public string Interceptions { get; set; }
        
        [DataMember(Name = "fld_goals_made")]
        public string FieldGoalsMade { get; set; }
        
        [DataMember(Name = "fld_goals_att")]
        public string FieldGoalsAttemps { get; set; }
        
        [DataMember(Name = "extra_pt_made")]
        public string ExtraPointsMade { get; set; }
        
        [DataMember(Name = "extra_pt_att")]
        public string ExtraPointAttempts { get; set; }
    }
}

