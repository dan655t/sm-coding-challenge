namespace sm_coding_challenge.Models.Extensions
{
    public static class DataResponseModelExtensions
    {
        public static bool IsValid(this DataResponseModel dataResponseModel)
        {
            return dataResponseModel.Rushing != null
                && dataResponseModel.Passing != null
                && dataResponseModel.Kicking != null
                && dataResponseModel.Receiving != null;
        }
    }
}